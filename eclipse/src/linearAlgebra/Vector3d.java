//Celine Tran 1938648
package linearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;

	public Vector3d(double x,double y,double z) {
		if (y<1||x<1||z<1) {
			throw new IllegalArgumentException("Variables must not be 0 or negative");
		}
		else {
		this.x=x;
		this.y=y;
		this.z=z;
	}
	}
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getZ() {
		return z;
	}
	
	 public String toString(){
		    return ("x:"+ getX()+ " y:"+getY()+ " z:"+getZ());
		  }
		  
	public double magnitude() {
		double magnitude= Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
		return magnitude;
	}

	public double dotProduct(Vector3d v2) {
		double dotProduct= (this.x*v2.x) + (this.y*v2.y) + (this.z*v2.z);
		return dotProduct;
	}
	
	public Vector3d add(Vector3d v3) {
		double addX= this.x+v3.x;
		double addY= this.y+v3.y;
		double addZ = this.z+v3.z;
		
		Vector3d v4 = new Vector3d(addX,addY,addZ);
		
		return v4 ;
	}
}

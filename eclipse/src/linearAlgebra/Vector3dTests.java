//Celine Tran 1938648
package linearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {


	@Test
	public void testGetMethods() {
		Vector3d vTest= new Vector3d(5,3,1);
		double expectedX=5.0;
		double actualX = vTest.getX();
		double expectedY=3.0;
		double actualY = vTest.getY();
		double expectedZ = 1.0;
		double actualZ = vTest.getZ();

		 assertEquals(expectedX,actualX);
		 assertEquals(expectedY,actualY);
		 assertEquals(expectedZ,actualZ);
	}
	
	@Test
	public void testMagnitudeMethod() {
		Vector3d vTestMag= new Vector3d(7,2,5);
		double expectedMag = 8.831760866327848;
		double actualMag = vTestMag.magnitude();
		assertEquals(expectedMag,actualMag);
	}
	
	@Test
	public void testDotProductMethod() {
		Vector3d vTestDot1= new Vector3d(7.5,2,5);
		Vector3d vTestDot2= new Vector3d(5,3,1);
		double expectedDot = 48.5;
		double actualDot = vTestDot1.dotProduct(vTestDot2);
		assertEquals(expectedDot,actualDot);
	}
	
	@Test
	public void testAddMethod() {
		Vector3d vTestAdd1= new Vector3d(2,1,4);
		Vector3d vTestAdd2= new Vector3d(6,4,3);
		double expAddX = 8.0;
		double expAddY = 5.0;
		double expAddZ = 7.0;
		Vector3d actualAdd = vTestAdd1.add(vTestAdd2);
		double actAddX = actualAdd.getX();
		double actAddY = actualAdd.getY();
		double actAddZ = actualAdd.getZ();
		assertEquals(expAddX,actAddX);
		assertEquals(expAddY,actAddY);
		assertEquals(expAddZ,actAddZ);
	}
}




